package model.data_structures;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;


public class NumberBag <T extends Number>{
	
	private HashSet<T> bag;
	
	public NumberBag(){
		this.bag = new HashSet<T>();
	}
	
	public NumberBag(ArrayList<T> data){
		this();
		if(data != null){
			for (T datum : data) {
				bag.add(datum);
			}
		}
		
	}
	
	
	public void addDatum(T datum){
		bag.add(datum);
	}
	
	public Iterator<T> getIterator(){
		return this.bag.iterator();
	}
	

}
