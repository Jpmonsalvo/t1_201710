package model.logic;

import java.util.Iterator;

import model.data_structures.NumberBag;

public class NumbersBagOperations {



	public double computeMean(NumberBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Double> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}


	public double getMax(NumberBag bag){
		double max = Double.MIN_VALUE;
		double value;
		if(bag != null){
			Iterator<Double> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}

		}
		return max;
	}
	public double getMin(NumberBag bag){
		double min = Double.MAX_VALUE;
		double value;
		if(bag != null){
			Iterator<Double> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( min > value){
					min = value;
				}
			}

		}
		return min;
	}
	public double sum(NumberBag bag){
		double sum = 0;

		if(bag != null){
			Iterator<Double> iter = bag.getIterator();
			while(iter.hasNext()){
				sum += iter.next();

			}
		}
		return sum;
	}
	public double quadraticSum(NumberBag bag){
		double qS = 0;

		if(bag != null){
			Iterator<Double> iter = bag.getIterator();
			while(iter.hasNext()){
				double temp=iter.next(); 
				qS += temp* temp;
			}
		}
		return qS;
	
	}
}

