package controller;

import java.util.ArrayList;

import model.data_structures.NumberBag;
import model.logic.NumbersBagOperations;

public class Controller {

	private static NumbersBagOperations model = new NumbersBagOperations();
	
	
	public static NumberBag createBag(ArrayList<Double> values){
         return new NumberBag(values);		
	}
	
	
	public static double getMean(NumberBag bag){
		return model.computeMean(bag);
	}
	
	public static double getMax(NumberBag bag){
		return model.getMax(bag);
	}
	public static double getMin(NumberBag bag){
		return model.getMin(bag);
	}
	public static double sum(NumberBag bag){
		return model.sum(bag);
	}
	public static double quadraticSum(NumberBag bag){
		return model.quadraticSum(bag);
	}
	
}
